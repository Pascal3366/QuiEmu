#ifndef PARSECONFIG_H
#define PARSECONFIG_H

#include <QString>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>

class parseconfig
{
public:
    parseconfig(QString configFile);

private:
    bool cd_image_set;
    bool disk_image_set;
    bool enable_kvm;
    bool CPUType_set;
    bool enable_spice;
    bool enable_graphics;
};

#endif // PARSECONFIG_H
