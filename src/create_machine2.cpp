#include "src/create_machine.h"
#include "src/create_machine2.h"
#include "src/create_machine3.h"
#include "ui_create_machine.h"
#include "ui_create_machine2.h"
#include "ui_create_machine3.h"
#include "src/choose_cd_image.h"
#include "ui_choose_cd_image.h"
#include "src/mainwindow.h"
#include <QDebug>

int ramSize;

create_machine2::create_machine2(QWidget *parent) : QWidget(parent), ui(new Ui::create_machine2) {
        ui->setupUi(this);
}

create_machine2::~create_machine2() {
    delete ui;
}

void create_machine2::on_ram_slider_valueChanged(int value) {
    ui->ram_size_txt->setText(QString::number(value));
}

void create_machine2::on_previous_btn_clicked() {
    this->hide();
    create_machine* Create_MachineNew = new create_machine();
    Create_MachineNew->window()->show();
}

void create_machine2::on_cancel_btn_clicked() {
    this->close();
    MainWindow *myMainWindow = new MainWindow();
    myMainWindow->show();
}

void create_machine2::on_next_btn_clicked() {
    ramSize = ui->ram_size_txt->text().toInt();
    if(ramSize==0) {
        QErrorMessage RamError(this);
        RamError.showMessage("Please specifiy a RAM Size.");
        RamError.exec();
    }
    else {
        myChoose_cd_imageWindow = new choose_cd_image();
        myChoose_cd_imageWindow->show();
        this->hide();
    }
}

void create_machine2::on_ram_size_txt_textChanged(const QString &ramSize) {
    ui->ram_slider->setValue(ramSize.toInt());
}
