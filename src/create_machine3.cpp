#include "src/mainwindow.h"
#include "ui_mainwindow.h"
#include "src/create_machine.h"
#include "src/create_machine2.h"
#include "src/create_machine3.h"
#include "src/choose_cd_image.h"
#include "src/writeconfig.h"
#include "ui_create_machine3.h"

extern QString MachineName;
extern int ramSize;
extern QString SystemType;
extern QString CD_DestinationFileName;
extern MainWindow *myMainWindow;
extern create_machine2 *myCreateMachine2Window;

create_machine3::create_machine3(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::create_machine3) {
        ui->setupUi(this);
    }

create_machine3::~create_machine3() {
    delete ui;
}

void create_machine3::on_finish_next_btn_clicked() {

    writeConfig ConfigWriter;

    if(ui->radio0->isChecked()) {
        myCreate_diskimageWindow = new create_diskimage();
        myCreate_diskimageWindow->show();
        DiskImage = myCreate_diskimageWindow->DestinationFileName;
    }
    if(ui->radio1->isChecked()) {
        if(ui->lineEdit->text().isEmpty()) {
            QErrorMessage PathError(this);
            PathError.showMessage("Please specifiy a Disk Image.");
            PathError.exec();
        }
        else {
            DiskImage = ui->lineEdit->text();
            this->close();
        }
    }
    if(!QDir(QString::fromUtf8(homedir) + "/.config/QuiEmu/Machines").exists()) {
        QDir().mkdir(QString::fromUtf8(homedir) + "/.config/QuiEmu");
        QDir().mkdir(QString::fromUtf8(homedir) + "/.config/QuiEmu/Machines");
    }
    configFile = QString::fromUtf8(homedir) + "/.config/QuiEmu/Machines/" + MachineName + ".json";
    ConfigWriter.write2Config(configFile,MachineName,SystemType,CD_DestinationFileName,DiskImage,ramSize,"q35");
    MainWindow *myMainWindow = new MainWindow();
    myMainWindow->show();
}

void create_machine3::on_file_choose_btn_clicked() {
    QFileDialog dialog2;
    dialog2.setFileMode(QFileDialog::ExistingFile);
    DiskImage = dialog2.getOpenFileName(this, tr("Select File"), QDir::currentPath(), tr("Disk Images (*.qcow2 *.vdi *.img)"));
    ui->lineEdit->setText(DiskImage);
}

void create_machine3::on_radio0_clicked() {
    ui->lineEdit->setEnabled(false);
    ui->file_choose_btn->setEnabled(false);
    ui->finish_next_btn->setText("Next");
}

void create_machine3::on_radio1_clicked() {
    ui->lineEdit->setEnabled(true);
    ui->file_choose_btn->setEnabled(true);
    ui->finish_next_btn->setText("Finish");
}

void create_machine3::on_previous_btn_clicked() {
    this->hide();
    choose_cd_image *myChooseCDImageWindow = new choose_cd_image();
    myChooseCDImageWindow->show();
}
