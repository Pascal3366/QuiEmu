#ifndef WRITECONFIG_H
#define WRITECONFIG_H

#include <string>
#include <QFile>
#include <QTextStream>

class writeConfig {
public:
   void write2Config(QString configFile, QString MachineName, QString SystemType, QString CD_DestinationFileName, QString DiskImage, int ramSize, QString MachineType);
   void write2Config(QString configFile, QString MachineName, QString SystemType, QString CD_DestinationFileName, QString DiskImage, int ramSize, QString MachineType, bool enable_kvm, QString CPU_Type, bool enable_spice, int spice_port, bool enable_graphics, QString GraphicsCard, bool enable_networking, QString NetworkInterface1, QString NetworkInterface2, QString NetworkInterface3, bool bypass_host_interface);
};

#endif // WRITECONFIG_H
